﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//
#pragma once
/*
#include <collection.h>
#include <ppltasks.h>*/
#include "MainPage.g.h"

namespace modernCX
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		MainPage();

	};
}
