#include "App.h"
#include "MainPage.xaml.h"
//
// App.cpp
// Implementation of the App class.
//

template <typename T>
T^ to_cx(winrt::Windows::Foundation::IUnknown const& from) {
	return safe_cast<T^>(reinterpret_cast<Platform::Object^>(winrt::get_abi(from)));
}

template <typename T>
T from_cx(Platform::Object^ from) {
	T to{ nullptr };
	winrt::check_hresult(reinterpret_cast<::IUnknown*>(from)->QueryInterface(winrt::put_abi(to)));
	return to;
}

namespace cx {
	using namespace ::Windows::Foundation;
}

namespace winrt {
	using namespace Windows::Foundation;
}

// https://github.com/Microsoft/cppwinrt/blob/master/Docs/Migrating%20C%2B%2B%20CX%20source%20code%20to%20C%2B%2B%20WinRT.md#registering-for-events-using-a-lambda
namespace modernCX
{
	/// <summary>
	/// Provides application-specific behavior to supplement the default Application class.
	/// </summary>
	using namespace winrt;
	using namespace winrt::Windows::ApplicationModel::Activation;
	using namespace winrt::Windows::Graphics::Imaging;
	using namespace winrt::Windows::Media::Ocr;
	using namespace winrt::Windows::Storage;
	using namespace winrt::Windows::Storage::Pickers;
	using namespace winrt::Windows::Storage::Streams;
	using namespace winrt::Windows::UI;
	using namespace winrt::Windows::UI::Core;
	using namespace winrt::Windows::UI::Xaml;
	using namespace winrt::Windows::UI::Xaml::Controls;
	using namespace winrt::Windows::UI::Xaml::Media;

	struct App : ApplicationT<App>
	{

		/// <summary>
		/// Invoked when the application is launched normally by the end user.  Other entry points
		/// will be used such as when the application is launched to open a specific file.
		/// </summary>
		/// <param name="e">Details about the launch request and process.</param>
		void OnLaunched(LaunchActivatedEventArgs const & e)
		{
			m_uiDispatcher = CoreWindow::GetForCurrentThread().Dispatcher();
			RequestedTheme(ApplicationTheme::Light);

			Window window = Window::Current();
			// https://github.com/Microsoft/cppwinrt/blob/master/Docs/Migrating%20C%2B%2B%20CX%20source%20code%20to%20C%2B%2B%20WinRT.md#registering-for-events-using-a-lambda
			// resuming from debugger does not call onLaunched, but using simulator directly does.
			if (e.PreviousExecutionState() == ApplicationExecutionState::Terminated)
			{
				// TODO: Restore the saved session state only when appropriate, scheduling the
				// final launch steps after the restore is complete

			}

			//		if (e.PrelaunchActivated() == false)
			// Do not repeat app initialization when the Window already has content,
			if (window.Content() == nullptr) {
				// Create a Frame to act as the navigation context and associate it with
				// a SuspensionManager key
				rootFrame = Frame();
				Suspending([=](auto &&, auto &&)
				{
					OnSuspending();
				});
				//*				 Place the frame in the current Window
				window.Content(rootFrame);// skipped when app is relaunched by simulator
			}

			// https://docs.microsoft.com/en-us/uwp/api/windows.ui.core.windowactivatedeventargs#Windows_UI_Core_WindowActivatedEventArgs_WindowActivationState
			// just ensure that the window is active
			// Ensure the current window is active
			window.Activate(); // https://docs.microsoft.com/en-us/uwp/api/windows.ui.core.windowactivatedeventargs#Windows_UI_Core_WindowActivatedEventArgs_WindowActivationState
			if (rootFrame.Content() == nullptr)
			{
				// http://stackoverflow.com/questions/18938508/why-is-thread-pool-work-item-executed-on-ui-thread
				// http://stackoverflow.com/questions/16533899/getting-access-violation-stemming-from-windows-ui-xaml-dll-in-c-sharp-winrt-app
				::Windows::UI::Xaml::Controls::Frame^ f=to_cx<::Windows::UI::Xaml::Controls::Frame>(rootFrame);
				//f->Navigate(::Windows::UI::Xaml::Interop::TypeName(MainPage::typeid));
				MainPage^ MP = ref new MainPage;
				f->Content = MP;
					rootFrame.Loaded([=](auto &&, auto &&)
				{
				});
			}


		}

		Frame rootFrame;
	protected:
	private:
		/// <summary>
		/// Invoked when application execution is being suspended.  Application state is saved
		/// without knowing whether the application will be terminated or resumed with the contents
		/// of memory still intact.
		/// </summary>
		/// <param name="sender">The source of the suspend request.</param>
		/// <param name="e">Details about the suspend request.</param>
		void OnSuspending() {
			//TODO: Save application state and stop any background activity
		}
		;
		/// <summary>
		/// Invoked when Navigation to a certain page fails
		/// </summary>
		/// <param name="sender">The Frame which failed navigation</param>
		/// <param name="e">Details about the navigation failure</param>
		void OnNavigationFailed(/*Platform::Object ^sender, Windows::UI::Xaml::Navigation::NavigationFailedEventArgs ^e*/)
		{
			//	throw ref new FailureException("Failed to load Page " + e->SourcePageType.Name);
		};
		CoreDispatcher m_uiDispatcher{ nullptr };

	};
}

using namespace modernCX;
/// <summary>
/// Initializes the singleton application object.  This is the first line of authored code
/// executed, and as such is the logical equivalent of main() or WinMain().
/// </summary>
#pragma warning(disable:4447) // Disable warning 'main' signature found without threading model for /ZW for APPX0702 Credit openCV
int __stdcall wWinMain(HINSTANCE, HINSTANCE, PWSTR, int)
{
	init_apartment();// https://github.com/Microsoft/cppwinrt/issues/6 /ZW compels manual init
					 // https://kennykerr.ca/2016/11/09/cppwinrt-working-with-implementations/
					 // https://moderncpp.com/2015/05/14/windows-universal-apps-with-standard-c/
	Application::Start([](auto &&)
	{
		make<App>();
	});
}
